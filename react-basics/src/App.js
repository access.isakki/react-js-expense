import React from 'react'
import Expenses from './components/Expenseitem/Expenses.js'
import { NewExpense } from './components/NewExpenses/NewExpense.js'

export const App = () => {


  const addExpensehandler = expense => {
    console.log ('In App.js');
    console.log (expense);
  }
  return (
    <div>
        <NewExpense  onAddExpense={addExpensehandler}/>
        <Expenses />
    </div>
  )
}