import React from 'react'
import './ExpenseItem.css'
import ExpenseDate from './ExpenseDate';

function Expenseitem(props){
    return(
        <div>
            <div className="expense-item">
                <ExpenseDate  date = {props.date}/>
                <div className="expense-item__description">
                    <h2 >{props.title}</h2>
                    <p className="expense-item__price">${props.amount}</p>
                </div>
            </div>
        </div>
    )
}

export default Expenseitem ;